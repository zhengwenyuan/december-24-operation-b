import java.util.Scanner;

//用户在主方法中输入n，
//自定义不同的方法实现判断n是否为奇数，偶数，素数，完数。在主方法中，输出结果。
public class a1 {
public static void main(String[] args) {
	Scanner a1 = new Scanner(System.in);
	System.out.println("请输入一个数：");
	int a = a1.nextInt();
	ji(a);
	ou(a);
	su(a);
	wan(a);
}
public static int ji(int a) {
	if (a%2!=0) {
		System.out.println(a+"是奇数");
	}else {
		System.out.println(a+"不是奇数");
	}
	return a;
}
public static int ou(int a) {
	if (a%2==0) {
		System.out.println(a+"是偶数");
	}else {
		System.out.println(a+"不是偶数");
	}
	return a;
}
public static int su(int a) {
	int s = 0;
	for (int i = 1; i <=a; i++) {
		if (a%i==0) {
			s++;
		}
	}
	if (s>2) {
		System.out.println(a+"不是素数");
	}else if (a==1) {
		System.out.println(a+"不是素数");
	}else {
		System.out.println(a+"是素数");
	}
	return a;
}
public static int wan(int a) {
	int s =0;
	for (int i = 1; i <a; i++) {
		if (a%i==0) {
			s+=i;
		}
		
	}
	if (a==s) {
		System.out.println(a+"是完数");
	}else {
		System.out.println(a+"不是完数");
	}
	return a;
}
}
